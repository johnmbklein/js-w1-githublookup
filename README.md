# GitHub Lookup

#### App to lookup github users' repos

#### By John Klein

## Description

A tool that displays repos and descriptions for the specified user.

## Known Bugs

None.

## Support and contact details

After you clone repo from github:

  Create a .env file in the project directory. Put the following text in the file, replacing "YOUR-API-KEY" with your GitHub API key:
  exports.apiKey = "YOUR-API-KEY";

  Run these commands in terminal in the project directory:
  $ npm install
  $ bower install
  $ gulp build --production
  $ gulp serve

Enjoy!

Get in touch with me with questions or comments or if you find a bug.

## Technologies Used

This page was built using Java, HTML, Velocity, and Spark. Testing was done using JUnit and FluentLenium. Database uses Postgres.

### License

Licensed under GPL. See license file for more information.

Copyright (c) 2016 John Klein
