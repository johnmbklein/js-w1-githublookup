var Lookup = require('./../js/github-lookup.js').Lookup;

var displayResults = function(response) {
  var output = "";
  output += "<h5>Search results for " + response[0].owner.login + "</h5><br>";
  for (var i = 0; i < response.length; i++) {
    output += "<p>Name: " + response[i].name + "<br>Description: " + response[i].description + "</p>";
  }
  $('#results').html(output);
};

$(document).ready(function(){
  $('#search').click(function() {
    var username = $('#user').val();
    $('#user').val("");
    var myLookup = new Lookup();
    myLookup.getRepos(username, displayResults);
  });
});
