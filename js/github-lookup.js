var apiKey = require('./../.env').apiKey;

exports.Lookup = function() {
};

exports.Lookup.prototype.getRepos = function(username, displayFunction){
  $.get('https://api.github.com/users/' + username + '/repos?access_token=' + apiKey).then(function(response){
    console.log(response);
    displayFunction(response);
  }).fail(function(error){
    console.log(error.responseJSON.message);
  });
};
